﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Spaceport.Nasa.Web.Services;
using Microsoft.AspNetCore.Routing;
using Spaceport.Nasa.Web.Entities;
using Microsoft.EntityFrameworkCore;

namespace Spaceport.Nasa.Web
{
    public class Startup
    {

        public Startup (IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration {get;set;}
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IGreeter, Greeter>();
            services.AddMvc();

            services.AddDbContext<SpaceportDbContext>(options => options
            .UseSqlite(Configuration.GetConnectionString("SpaceportSqLiteConnection")));
            services.AddScoped<ISpaceshipData, SqlSpaceshipData>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
        IHostingEnvironment env,
        IGreeter greeter,
        SpaceportDbContext spaceportDbContext)
        {
            spaceportDbContext.Database.EnsureCreated();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseMvc(ConfigureRoutes);

            app.UseDeveloperExceptionPage();
            
            var greeting = greeter.GetGreeting();
            app.Run(async (context) =>
            {
                await context.Response.WriteAsync(greeting);
            });
        }

        private void ConfigureRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Default", "{controller=Home}/{action=Index}/{id?}");
        }
    }
}
