using System;
using Microsoft.AspNetCore.Mvc;
using Spaceport.Nasa.Web.ViewModels;
using Spaceport.Nasa.Web.Services;
using Spaceport.Nasa.Web.Entities;

namespace Spaceport.Nasa.Web.Controllers
{
    public class HomeController : Controller
    {
        public ISpaceshipData _spaceshipData;
        public IGreeter _greeter;
        public HomeController (ISpaceshipData spaceshipData,
        IGreeter greeter)
        {
            _spaceshipData = spaceshipData;
            _greeter = greeter;
        }
        public ViewResult Index ()
        {
            var model = new HomePageViewModel();
            model.Spaceships = _spaceshipData.GetAll();
            model.CurrentGreeting = _greeter.GetGreeting();
            return View(model);
        }

        public IActionResult Details(int id)
        {
            var model = _spaceshipData.Get(id);
            if(model == null)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create (SpaceshipEditViewModel model)
        {
            if(ModelState.IsValid) 
            {
                var spaceship = new Spaceship();
                spaceship.Name = model.Name;
                spaceship.Type = model.Type;

                _spaceshipData.Add(spaceship);

                return RedirectToAction("Details", new {Id = spaceship.Id});
            }
            return View();
        }
    }
}