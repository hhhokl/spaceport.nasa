using Microsoft.AspNetCore.Mvc;

namespace Spaceport.Nasa.Web.Controllers
{
    [Route("info/[controller]/[action]")]
    public class AboutController
    {
        public string aplusplus()
        {
            int a = 1;
            var b = a++;
            return string.Format("initial a = 1. After increment operator a = {0} and b = {1}", a, b);
        }

        public string plusplusa()
        {
            int a = 1;
            var b = ++a;
            return string.Format("initial a = 1. After decrement operator a = {0} and b = {1}", a, b);
        }
        public string minusminusa()
        {
            int a = 1;
            var b = --a;
            return string.Format("initial a = 1. After --a operator a = {0} and b = {1}", a, b);
        }
        public string aminusminus()
        {
            int a = 1;
            var b = a--;
            return string.Format("initial a = 1. After a-- operator a = {0} and b = {1}", a, b);
        }
    }
}