using System;
using System.Collections.Generic;
using System.Linq;
using Spaceport.Nasa.Web.Entities;

namespace Spaceport.Nasa.Web.Services
{
    public interface ISpaceshipData
    {
        IEnumerable<Spaceship> GetAll();
        Spaceship Get(int id);
        void Add (Spaceship newSpaceship);
    }

    public class SqlSpaceshipData : ISpaceshipData
    {
        private SpaceportDbContext _context;

        public SqlSpaceshipData(SpaceportDbContext context)
        {
            _context = context;
        }

        public void Add(Spaceship newSpaceship)
        {
            _context.Add(newSpaceship);
            _context.SaveChanges();
        }

        public Spaceship Get(int id)
        {
            return _context.Spaceships.FirstOrDefault(_ => _.Id == id);
        }

        public IEnumerable<Spaceship> GetAll()
        {
            return _context.Spaceships.ToList();
        }
    }
    public class InMemorySpaceshipData : ISpaceshipData
    {
        static List<Spaceship> _spaceships;
        static InMemorySpaceshipData()
        {
            _spaceships = new List<Spaceship>
            {
                new Spaceship {Id = 1, Name = "Falcon"},
                new Spaceship {Id = 2, Name = "MKS"},
                new Spaceship {Id = 3, Name = "Chuppie"}
            };
        }
        public IEnumerable<Spaceship> GetAll()
        {
            return _spaceships;
        }

        public void Add(Spaceship newSpaceship)
        {
            newSpaceship.Id = _spaceships.Max(_ => _.Id) + 1;
            _spaceships.Add(newSpaceship);
        }

        public Spaceship Get(int id)
        {
            return _spaceships.FirstOrDefault(_ => _.Id == id);
        }
    }
}
