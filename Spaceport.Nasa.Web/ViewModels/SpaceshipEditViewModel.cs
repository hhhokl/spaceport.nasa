using System.ComponentModel.DataAnnotations;
using Spaceport.Nasa.Web.Entities;

namespace Spaceport.Nasa.Web.ViewModels
{
    public class SpaceshipEditViewModel
    {
        [Required, MaxLength(80)]
        public string Name {get;set;}
        public SpaceshipType Type {get;set;}
    }
}