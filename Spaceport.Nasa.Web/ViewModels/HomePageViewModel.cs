using System.Collections.Generic;
using Spaceport.Nasa.Web.Entities;

namespace Spaceport.Nasa.Web.ViewModels
{
    public class HomePageViewModel
    {
        public IEnumerable<Spaceship> Spaceships { get; set; }
        public string CurrentGreeting { get; set; }
    }
}