using Microsoft.EntityFrameworkCore;

namespace Spaceport.Nasa.Web.Entities
{
    public class SpaceportDbContext : DbContext
    {
        public SpaceportDbContext(DbContextOptions<SpaceportDbContext> options) : base(options)
        {
        }
        
        public DbSet<Spaceship> Spaceships {get;set;}
    }
}