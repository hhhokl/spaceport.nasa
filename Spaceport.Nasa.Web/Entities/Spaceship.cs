using System.ComponentModel.DataAnnotations;

namespace Spaceport.Nasa.Web.Entities
{
    public enum SpaceshipType
    {
        Military,
        Intergalactic,
        Orbital,
        Transport
    }
    public class Spaceship
    {
        public int Id {get;set;}

        [Required, MaxLength(80)]
        public string Name {get;set;}
        public SpaceshipType Type { get;set;}
    }
}   